SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `GT_db` ;
USE `GT_db` ;

-- -----------------------------------------------------
-- Table `GT_db`.`UserInfo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GT_db`.`UserInfo` (
  `userID` VARCHAR(30) NOT NULL,
  `status_login` INT NOT NULL,
  `sha_pw` TEXT NULL,
  `publickey_user` TEXT NULL,
  `privatekey_server` TEXT NULL,
  `lastcalltime` DATETIME NULL,
  PRIMARY KEY (`userID`),
  UNIQUE INDEX `userID_UNIQUE` (`userID` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GT_db`.`FileList`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GT_db`.`FileList` (
  `userID` VARCHAR(30) NOT NULL,
  `filename` VARCHAR(200) NOT NULL,
  `pixelsize_x` INT NULL,
  `pixelsize_y` INT NULL,
  `env_max_x` DOUBLE NULL,
  `env_min_x` DOUBLE NULL,
  `env_max_y` DOUBLE NULL,
  `env_min_y` DOUBLE NULL,
  `CRS_decode` TEXT NULL,
  `split_size` INT NULL,
  `split_num_x` INT NULL,
  `split_num_y` INT NULL,
  `status` INT NULL,
  `residule` INT NULL,
  PRIMARY KEY (`userID`, `filename`),
  CONSTRAINT `filelist_userinfo`
    FOREIGN KEY (`userID`)
    REFERENCES `GT_db`.`UserInfo` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
